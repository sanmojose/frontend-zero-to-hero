# Web Zero to Hero
This project's goal is to teach a friend of mine as I update my knowledge of the Web starting on Frontend

## Roadmap
1. Very very basics of the web: addEventListener, setInterval, random, arrays
1. Tests
1. Small projects comming: tarot, hangman and more comming

## Lesson1
1. Plenty on spaghetti code
1. Some play on event listeners and "mistakes"
1. setInterval => countdown
1. array usage => querySelectorAll, hardcoded array 

## Lesson 2
1. Practice with objects, arrays and functions