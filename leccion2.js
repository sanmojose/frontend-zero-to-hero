
const fiestaFavorita = 'San Juan'
const usuario = {
    nombre: 'Jose',
    apellido: 'Sánchez',
    profesion: 'programador fullstack',
    intereses: ['ES23', 'vuejs', 'nodejs', 'docker', 'nginx'],
    fiestaFavorita,
}

const { nombre: n, fiestaFavorita: fF , ...testUsuario } = usuario
const testUsuario2 = { ...usuario, tratamiento: 'Ms.', nombre: 'María' }
testUsuario2.fiestaFavorita = 'Halloween'

let diaEspecial1 = usuario?.fiestaFavorita
diaEspecial1 ??= 'Navidad' 
let diaEspecial2 = testUsuario?.fiestaFavorita
diaEspecial2 ??= 'Navidad' 
let diaEspecial3 = testUsuario2.fiestaFavorita
console.log('Día especial', diaEspecial1, diaEspecial2, diaEspecial3)


const serializadoNombre = ({ tratamiento: t = 'Mr.', nombre: n, apellido: a, profesion: p}) => `${t} ${n} ${a} es ${p}`
const serializadoAnonimo = ({ tratamiento = 'Mx.', profesion }) => `${tratamiento} Anonynous es una persona curiosa que se dedica a ${profesion}`

const serializadoUsuario = usuario => { 
    let fn = usuario.nombre ? serializadoNombre : serializadoAnonimo
    return fn(usuario)
}

console.log( [usuario, testUsuario, testUsuario2].forEach( u => console.log(serializadoUsuario(u))))

const dias1 = ['lunes', 'martes']
const dias2 = ['miércoles', 'jueves']
const dias3 = ['viernes', 'sábado', 'domingo']
const algunosDias = [...dias1, ...dias3]
const todosLosDias = [dias1, dias2, dias3].flat();
const ultimoDiaLibre = todosLosDias[length-1] || 'domingo'

const esDiaLaboral = dia => dia.endsWith('es')

const laboralesToHtmlList = lista => 
    '<ul>'
    + lista
    .filter( esDiaLaboral )
    .map( e => `<li>${e}</li>` )
    .reduce( (a,b) => `${a}\n\t${b}`, '\t')
    + '\n</ul>'

const listaHtml = laboralesToHtmlList(todosLosDias)
console.log('listaHtml', listaHtml);
