import {
    hablarConI,
    contarA,
    esPalindromo,
    esPrimo,
}
from '../ejercicio1.js'


let tests = []

tests.push( () => {
    console.assert( hablarConI('Hola') === 'Hili', 'Se esperaba Hili')
    console.assert( hablarConI('aeiou') === 'iiiii', 'Se esperaba iiii')
    console.assert( hablarConI('murciélago') === 'mirciíligi', 'Se esperaba mirciiligi')
    console.assert( hablarConI('Árbol') === 'Írbil', 'Se esperaba Írbil')
    console.assert( hablarConI('mañana') === 'miñini', 'Se esperaba miñini')
    console.assert( hablarConI('Lorem ipsum') === 'Lirim ipsim', 'Se esperaba Lirim ipsim')
    console.assert( hablarConI('AEIOU') === 'IIIII', 'Se esperaba IIIII')
});

tests.push( () => {
    let resultado = contarA('Jose')
    console.assert( typeof resultado === 'number', 'Tipo debe ser Number' )
    console.assert( resultado === 0, 'Se esperaban 0 "a"es'  )
    console.assert( contarA('Hola') === 1, 'Se esperaban 1 "a"es' )
    console.assert( contarA('Bala') === 2 , 'Se esperaban 2 "a"es' )
    console.assert( contarA('aaaaa') === 5 , 'Se esperaban 5 "a"es' )
    console.assert( contarA('Mañana por la mañana iré a la playa') === 11 , 'Se esperaban 11 "a"es' )
})

tests.push( () => {
    console.assert( !esPalindromo('hola'), 'Hola no es palíndromo')
    console.assert( !esPalindromo('Cristian'), 'Cristian no es palíndromo')
    console.assert( esPalindromo('kayak'), 'Kayak sí es palíndromo')
    console.assert( esPalindromo('radar'), 'Radar es palíndromo')
    console.assert( esPalindromo('holaaloh'), 'holaaloh sí es palíndromo')
    console.assert( esPalindromo('holaloh'), 'holaloh sí es palíndromo' )
})

tests.push( () => {
    console.assert( esPrimo('8') === undefined, 'Los strings deber retornar undefined' )
    console.assert( esPrimo([]) === undefined, 'Los arrays deber retornar undefined' )
    console.assert( esPrimo({}) === undefined, 'Los objetos deber retornar undefined' )
    try {
        esPrimo();
        console.assert( false , 'Lanzar la función sin parámetros debe lanzar un error' )
    } catch (e) {}
    console.assert( esPrimo(2), 'El número 2 sí es un número primo' )
    console.assert( !esPrimo(8), 'El número 8 no es un número primo (2*2*2)' )
    console.assert( esPrimo(23), 'El número 23 sí es un número primo' )
    console.assert( !esPrimo(51), 'El número 51 no es un número primo (3*17)' )
    console.assert( !esPrimo(529), 'El número 529 no es un número primo (23*23)' )
    console.assert( esPrimo(541), 'El número 541 sí es un número primo' )
    console.assert( esPrimo(7919), 'El número 7919 sí es un número primo' )
    console.assert( esPrimo(1299709), 'El número 1299709 sí es un número primo' )
    console.assert( !esPrimo(1299710), 'El número 1299710 no es un número primo' )
})


tests.forEach(t => { try { t() } catch (e) { console.log(e.message); } })